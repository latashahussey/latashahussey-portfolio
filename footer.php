<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

    <?php get_sidebar( 'footerfull' ); ?>

        <div class="wrapper graydark-bg" id="wrapper-footer">

            <div class="<?php echo esc_attr( $container ); ?>">

                <div class="row">

                    <div class="col-md-12">

                        <footer class="site-footer" id="colophon">

                            <div class="site-info">
                                <div class="row">
                                    <div class="col-md-6 text-center pt-3">
                                        <!-- Social Media Icons Here-->
										<div class="soc" data-size="60px" data-radius="circle" data-buttoncolor="#F4F1F9" data-iconcolor="#47434D">
											<a href="http://linkedin.com/in/latashahussey" class="soc-linkedin" title="LinkedIn" target="_blank"></a>
											<a href="http://twitter.com/latashahussey" class="soc-twitter" title="Twitter" target="_blank"></a>
											<a href="http://github.com/latashahussey" class="soc-github" title="GitHub" target="_blank"></a>
										</div>
                                    </div>
                                    <div class="col-md-6 gray-fg">
                                        <!-- Copyright Info -->
                                        <p class="white-fg">LaTasha Hussey &copy; 2018. All Rights Reserved.</p>
                                        <p class="text-small">Designed by <a href="https://www.linkedin.com/in/cristinaestrod/?locale=en_US" class="pink-fg" target="_blank">Cristina Estévez Designs.</a>
                                        Coded by Me, my MacBook, and whole lotta love.  Built with <a class="pink-fg" href="https://understrap.com/" target="_blank">Understrap</a>.
                                        </p>
                                    </div>
                                </div>


                            </div>
                            <!-- .site-info -->

                        </footer>
                        <!-- #colophon -->

                    </div>
                    <!--col end -->

                </div>
                <!-- row end -->

            </div>
            <!-- container end -->

        </div>
        <!-- wrapper end -->

        </div>
        <!-- #page -->

        <?php wp_footer(); ?>

        <script id="__bs_script__">//<![CDATA[
            document.write("<script async src='http://HOST:8005/browser-sync/browser-sync-client.js?v=2.18.1
        3'><\/script>".replace("HOST", location.hostname));
        //]]></script>
            </body>

            </html>
