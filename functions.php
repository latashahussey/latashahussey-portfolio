<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Load functions to secure your WP install.
 */
require get_template_directory() . '/inc/security.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/pagination.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/custom-comments.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';

/**
 * Load WooCommerce functions.
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Load Editor functions.
 */
require get_template_directory() . '/inc/editor.php';

add_action( 'init', 'cd_add_editor_styles' );
/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
function cd_add_editor_styles() {
 add_editor_style(get_stylesheet_uri());
}

// Creates Education Skills
function tashas_quotes_init() {
    $args = array(
      'label' => 'Tasha\'s Quotes',
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'tashas-quotes'),
        'query_var' => true,
        'menu_icon' => 'dashicons-format-quote'
        );
    register_post_type( 'tashas-quotes', $args );
}
add_action( 'init', 'tashas_quotes_init' );


function my_theme_scripts() {

    //Enqueue jQuery for Skills Chart
    wp_enqueue_script( 'easyPieChart', get_template_directory_uri() . '/js/easyPieChart.js', array( 'jquery' ), '1.0.0', true );
    wp_enqueue_script( 'skills', get_template_directory_uri() . '/js/skills.js', array( 'jquery' ), '1.0.0', true );

    // Enqueue FontAwesome v5 Alpha
    wp_enqueue_script( 'fontawesome', get_template_directory_uri() . '/js/fontawesome.js');
    wp_enqueue_script( 'regular', get_template_directory_uri() . '/js/regular.js');
    wp_enqueue_script( 'light', get_template_directory_uri() . '/js/light.js');

    //Enqueue DevIcons
    wp_enqueue_style( 'devicons', 'https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css');

    //Enqueue Social Media Icons (soc.js)
    wp_enqueue_style( 'soccss', get_template_directory_uri() . '/css/soc.min.css');
    wp_enqueue_script( 'socjs', get_template_directory_uri() . '/js/soc.min.js');

}

add_action( 'wp_enqueue_scripts', 'my_theme_scripts' );
