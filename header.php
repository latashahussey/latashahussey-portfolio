<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>



    </head>

    <body <?php body_class(); ?>>

        <div class="hfeed site" id="page">

                <div id="intro"  class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 col-xs-2 p-4">
                            <!--Branding-->
                            <h4 class="text-left"><strong>LaTasha</strong> Hussey</h4>
                        </div>

                        <div class="col-md-8 ">
                            <!--Main Menu-->
                            <nav class="nav justify-content-end p-4">
                                <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff
                      <div class="skip-link screen-reader-text">
                    <a title="Skip to content" href="#content">Skip to content</a> </div>*/ ?>
                                    <?php wp_nav_menu( array( 'container_class' => 'main-menu', 'theme_location' => 'primary' ) ); ?>
                            </nav>
                        </div>
                    </div>

                <!--- Intro -->
                <div class="row justify-content-center">
                    <h1>LaTasha Hussey</h1>
                </div>
                <div class="my-title row justify-content-center">
                    <h3>Educator | Trainer | Web Developer </h3>
                </div>
            </div>
            <!--- #end container-->
