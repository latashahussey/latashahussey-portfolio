<?php /* Template Name: Front Page Template */
/**
 * The template for displaying the Front Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod('understrap_container_type');
$sidebar_pos = get_theme_mod('understrap_sidebar_position');

?>

    <div class="wrapper" id="page-wrapper">
        <main class="site-main" id="main">
            <div id="about-me" class="container">
                <div class="row">
                    <!--ABOUT ME-->
                    <div id="bio" class="col-lg-6 col-md-6 pl-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>About Me</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="text-left">Hi, I'm LaTasha!</h2>
                                <hr class="pull-left" />
                            </div>
                        </div>
                        <div class="row">
                            <!-- Short Bio-->
                            <div class="col-md-12 pb-5">
                                <p>I've taught in higher education for the past 14 years, and from training hundreds of faculty, I know what it takes to create an engaging, fun environment for online and face-to-face courses.</p>
                                <p>Because I am so passionate about e-learning and development, I even wrote a book to help faculty streamline their online courses.</p>
                                <p>As a web developer, I love to make code flow like poetry. I have experience developing custom themes for WordPress, writing vanilla JavaScript, and creating good old-fashioned, responsive websites using just HTML and CSS.</p>
                                <p>When I’m not overdosing on code, you can find me binge-watching my favorite crime shows or drooling over the latest tech gadget.</p>
                            </div>
                        </div>
                    </div>
                    <!--Bio Pic, CTAs, and Social Media--->
                    <div id="social-cta" class="col-lg-6 col-md-6 col-sm-pull-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!---Bio Pic-->
                                <img class="rounded-circle" src="<?php echo get_template_directory_uri(); ?>/img/src/latasha-hussey-about.jpg">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <ul>
                                    <li>
                                        <!--- Portfolio CTA-->
                                        <a href="#portfolio">Check out my portfolio</a>
                                    </li>
                                    <li>
                                        <!---Book CTA-->
                                        <a href="http://latashahussey.com/book" target="_blank">Take a peek at my book</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row shift-right">
                            <div class="col-md-12">
                                <h6 class="text-left">Follow Me</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Social Media Links -->
                                <div class="soc shift-right" data-size="50px" data-radius="circle" data-buttoncolor="#47434D" data-iconcolor="#F4F1F9">
                                    <a href="http://linkedin.com/in/latashahussey" class="soc-linkedin" title="LinkedIn" target="_blank"></a>
                                    <a href="http://twitter.com/latashahussey" class="soc-twitter" title="Twitter" target="_blank"></a>
                                    <a href="http://github.com/latashahussey" class="soc-github" title="GitHub" target="_blank"></a>
                                </div>
                            </div>
                        </div>
                        <div class="row shift-right">
                            <div class="col-md-12">
                                <!--Hire Me CTA to Contact Form-->
                                <a href="#contact">
                                    <button class="btn btn-primary" type="button">Hire Me!</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- about me .row end-->
            </div>
            <!--- about me .container end-->

            <!--FIRST JUMBO QUOTE-->
            <div id="first-quote" class="jumbotron jumbotron-fluid edge--top edge--bottom--reverse">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2"><img class="pull-right" src="<?php echo get_template_directory_uri(); ?>/img/src/quotation-mark-green.png"></div>
                        <div class="col-md-10">
                            <blockquote class="text-center">
                                As an educator, I give my students something no one can take away: Knowledge.
                            </blockquote>
                        </div>
                    </div>
                </div>
                <!--- jumbotron.container end -->
            </div>
            <!--- .jumbotron end --->

            <!-- EDUCATION SKILLS-->
            <div id="education-skills" class="container">
                <div class="row skills text-center">
                    <div class="col-md-12">
                        <h4>What I Do</h4>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-12">
                        <h3>Education Skills</h3>
                        <hr />
                    </div>
                </div>
                <div id="skills" class="row text-center">
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--Public Speaking-->
                        <div class="chart" data-percent="90"><img src="<?php echo get_template_directory_uri(); ?>/img/src/public-speaking-icon.png"></div>
                        <p>Public Speaking</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--Mentoring-->
                        <div class="chart" data-percent="70"><img src="<?php echo get_template_directory_uri(); ?>/img/src/mentoring-icon.png"></div>
                        <p>Mentoring</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--Teaching-->
                        <div class="chart" data-percent="100"><img src="<?php echo get_template_directory_uri(); ?>/img/src/teaching-icon.png"></div>
                        <p>Teaching</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--Course Development-->
                        <div class="chart" data-percent="70"><img src="<?php echo get_template_directory_uri(); ?>/img/src/course-development-icon.png"></div>
                        <p>Course Development</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--E-Learning-->
                        <div class="chart" data-percent="90"><img src="<?php echo get_template_directory_uri(); ?>/img/src/elearning-icon.png"></div>
                        <p>E-Learning</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--Instructional Technology-->
                        <div class="chart" data-percent="95"><img src="<?php echo get_template_directory_uri(); ?>/img/src/instructional-technology-icon.png"></div>
                        <p>Instructional Technology</p>
                    </div>
                </div>
                <!-- end Ed Skills .row=-->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#experience">
                            <button class="btn btn-info btn-xl" type="button">Review My Experience</button>
                        </a>
                    </div>
                </div>
            </div>
            <!-- end Ed Skills .container -->
            <!--SECOND QUOTE-->
            <div id="second-quote" class="jumbotron jumbotron-fluid edge--top--reverse edge--bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 float-sm-left"><img class="pull-right" src="<?php echo get_template_directory_uri(); ?>/img/src/quotation-mark-green.png"></div>
                        <div class="col-md-10">
                            <blockquote class="text-center">
                                As a developer, there is no greater satisfaction than knowing you have created something useful that someone else can use.
                            </blockquote>
                        </div>
                    </div>
                </div>
                <!--- jumbotron.container end -->
            </div>
            <!--- .jumbotron end =-->

            <!-- DEVELOPMENT SKILLS-->
            <div id="development-skills" class="container">
                <div class="row text-center">
                    <div class="col-md-12">
                        <h4>What I Do</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Development Skills</h3>
                        <hr />
                    </div>
                </div>
                <div id="skills" class="row text-center">
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--HTML5-->
                        <div class="chart" data-percent="90"><img src="<?php echo get_template_directory_uri(); ?>/img/src/html5-outlined-icon.png"></div>
                        <p>HTML5</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--CSS3-->
                        <div class="chart" data-percent="85"><img src="<?php echo get_template_directory_uri(); ?>/img/src/css3-outlined-icon.png"></div>
                        <p>CSS3</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--JavaScript-->
                        <div class="chart" data-percent="60"><img src="<?php echo get_template_directory_uri(); ?>/img/src/javascript-outlined-icon.png"></div>
                        <p>JavaScript</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--PHP-->
                        <div class="chart" data-percent="60"><img src="<?php echo get_template_directory_uri(); ?>/img/src/php-outlined-icon.png"></div>
                        <p>PHP</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--MySQL-->
                        <div class="chart" data-percent="60"><img src="<?php echo get_template_directory_uri(); ?>/img/src/mysql-outlined-icon.png"></div>
                        <p>MySQL</p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <!--WordPress-->
                        <div class="chart" data-percent="70"><img src="<?php echo get_template_directory_uri(); ?>/img/src/wordpress-outlined-icon.png"></div>
                        <p>WordPress Development</p>
                    </div>
                </div>
                <!-- end Dev Skills .row=-->
                <!-- My Toolkit-->
                <div id="toolkit" class="row">
                    <div class="col-md-3">
                        <h3>My Toolkit</h3>
                    </div>
                    <div class="col-md-9">
                        <ul class="list-inline">
                            <li class="list-inline-item"><i class="fal fa-check" aria-hidden="true"></i>macOS</li>
                            <li class="list-inline-item"><i class="fal fa-check" aria-hidden="true"></i>Photoshop</li>
                            <li class="list-inline-item"><i class="fal fa-check" aria-hidden="true"></i>Atom</li>
                            <li class="list-inline-item"><i class="fal fa-check" aria-hidden="true"></i>Chrome DevTools</li>
                            <li class="list-inline-item"><i class="fal fa-check" aria-hidden="true"></i>gulp</li>
                            <li class="list-inline-item"><i class="fal fa-check" aria-hidden="true"></i>Git and GitHub</li>
                            <li class="list-inline-item"><i class="fal fa-check" aria-hidden="true"></i>GitLab</li>
                        </ul>
                    </div>
                </div>
                <!-- end My Toolkit .row-->

                <!--- CTA - View my Portfolio --->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#portfolio">
                            <button class="btn btn-info btn-xl" type="button">View My Portfolio</button>
                        </a>
                    </div>
                </div>
                <!-- end  View my Portfolio .row -->
            </div>
            <!-- end Dev Skills .container -->


            <!-- MY EXPERIENCE-->
            <?php // Use get_post() to grab the My Experience page title and content
			      // Reference: https://developer.wordpress.org/reference/functions/get_post/
			                  $post = get_post(20, ARRAY_A);
			                  $title = $post['post_title'];
			                  $content = do_shortcode($post['post_content']);
			                 ?>
                <div id="experience" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-center">Résumé</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!--Post title-->
                            <h3 class="entry-title" itemprop="headline"><?php echo $title; ?></h3>
                        </div>
                    </div>
                    <!-- Display My Experience Page Content-->
                    <div class="row page">
                        <div class="col-md-12">
                            <div id="entry-content">
                                <div class="entry-content" itemprop="text">
                                    <?php echo $content; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end My Experience content .row-->
                </div>
                <!--- end My Experience .container =-->

                <!--CTA Download full resume -->
                <div id="resume" class="jumbotron jumbotron-fluid  edge--top edge--bottom--reverse">
                    <div class="container text-center">
                        <div class="row">

                            <div class="col-md-12">
                                <h4><a href="<?php echo get_template_directory_uri(); ?>/downloads/Resume-for-LaTasha-Hussey.pdf" target="_blank"><img src= "<?php echo get_template_directory_uri(); ?>/img/src/download-resume-icon.png"/>Download full résumé</a></h4>
                            </div>

                        </div>
                    </div>
                </div>
                <!--- jumbotron.container end -->


                <!-- PORTFOLIO-->
                <div id="portfolio" class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h4>My Work</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Portfolio</h3>
                            <hr />
                        </div>
                    </div>
                    <!-- Portfolio Items -->

                    <!--Portfolio Row #1-->
                    <div class="row">
                        <div class="card col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <img alt="Readable" class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/src/readable-app-project.png">
                            <div class="slanted-title-box">
                                <h6 class="card-title">Readable</h6>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Content and comment web app using React and Redux with React Router.</p>
                                <div class="card-footer"><a class="btn btn-info" href="https://hussey-readable-frontend.herokuapp.com/" target="_blank">View Live Site</a> <a class="btn btn-link teal-fg" href="https://gitlab.com/latashahussey/reactnd-project-readable-hussey/" target="_blank">Check code &gt;</a></div>
                            </div>
                            <!-- .card-body -->
                        </div>
                        <div class="card col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <img alt="MyReads" class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/src/my-reads-app-project.png">
                            <div class="slanted-title-box">
                                <h6 class="card-title">MyReads</h6>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Digital bookshelf created using React with React Router.</p>
                                <div class="card-footer"><a class="btn btn-info" href="http://my-reads-app-hussey.surge.sh" target="_blank">View Live Site</a> <a class="btn btn-link teal-fg" href="https://github.com/latashahussey/my-reads-app/" target="_blank">Check code &gt;</a></div>
                            </div>
                            <!-- .card-body -->
                        </div>
                        <!-- .card-->
                        <div class="card col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <img alt="Jane Doette" class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/src/latasha-hussey-project.jpg">
                            <div class="slanted-title-box">
                                <h6 class="card-title">LaTasha Hussey</h6>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Developed my responsive personal portfolio page using the Understrap theme framework.</p>
                                <div class="card-footer"><a class="btn btn-info disabled" href="#" target="_blank">View Live Site</a> <a class="btn btn-link teal-fg" href="https://gitlab.com/latashahussey/latashahussey-portfolio" target="_blank">Check code &gt;</a></div>
                            </div>
                            <!-- .card-body -->
                        </div>


                        <!--Portfolio Row #2-->
                        <!-- .card-->
                        <div class="card col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <img alt="Jane Doette" class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/src/jane-doette-project.jpg">
                            <div class="slanted-title-box">
                                <h6 class="card-title">Jane Doette</h6>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Fully responsive personal portfolio page built with the Bootstrap framework.</p>
                                <div class="card-footer"><a class="btn btn-info" href="https://latashahussey.github.io/jane-doette-portfolio/" target="_blank">View Live Site</a> <a class="btn btn-link teal-fg" href="https://github.com/latashahussey/jane-doette-portfolio" target="_blank">Check code &gt;</a></div>
                            </div>
                            <!-- .card-body -->
                        </div>
                        <!-- .card-->
                        <div class="card col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <img alt="Micah Online" class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/src/micah-online-project.jpg">
                            <div class="slanted-title-box">
                                <h6 class="card-title">Micah Online</h6>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Personal website of local comedian, Micah. Customized CSS for existing WordPress theme.
                                </p>
                                <div class="card-footer"><a class="btn btn-info" href="http://micahonline.com/" target="_blank">View Live Site</a></div>
                            </div>
                            <!-- .card-body -->
                        </div>
                        <!-- .card-->
                        <div class="card col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <img alt="Move Companion" class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/src/move-companion-project.jpg">
                            <div class="slanted-title-box">
                                <h6 class="card-title">Move Companion</h6>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Web application that pulls data from three APIs to modify the DOM and display the information.</p>
                                <div class="card-footer"><a class="btn btn-info" href="https://latashahussey.github.io/ajax-move-companion/" target="_blank">View Live Site</a> <a class="btn btn-link teal-fg" href="https://github.com/latashahussey/ajax-move-companion" target="_blank">Check code &gt;</a></div>
                            </div>
                            <!-- .card-body -->
                        </div>


                        <!--Portfolio Row #3-->
                        <!-- .card-->
                        <div class="card col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <img alt="Interactive Résumé" class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/src/interactive-resume-project.png">
                            <div class="slanted-title-box">
                                <h6 class="card-title">Interactive Résumé</h6>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Application that reads all data from a JSON file and then modifies the DOM to display the information.</p>
                                <div class="card-footer"><a class="btn btn-info" href="https://latashahussey.github.io/frontend-nanodegree-resume/" target="_blank">View Live Site</a> <a class="btn btn-link teal-fg" href="https://github.com/latashahussey/frontend-nanodegree-resume/"
                                    target="_blank">Check code &gt;</a></div>
                            </div>
                            <!-- .card-body -->
                        </div>
                        <!-- .card-->
                        <div class="card col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <img alt="Bug Bites" class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/src/bug-bites-project.jpg">
                            <div class="slanted-title-box">
                                <h6 class="card-title">Bug Bites!</h6>
                            </div>
                            <div class="card-body">
                                <p class="card-text">An HTML5 canvas game developed using the best practices in object-oriented JavaScript.</p>
                                <div class="card-footer"><a class="btn btn-info" href="https://latashahussey.github.io/frontend-nanodegree-arcade-game/" target="_blank">View Live Site</a> <a class="btn btn-link teal-fg" href="https://github.com/latashahussey/frontend-nanodegree-arcade-game/"
                                    target="_blank">Check code &gt;</a></div>
                            </div>
                            <!-- .card-body -->
                        </div>
                        <!-- .card-->

                        <div class="card col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <img alt="Accelerate Marketing" class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/src/accelerate-marketing-project.jpg">
                            <div class="slanted-title-box">
                                <h6 class="card-title">Accelerate Marketing</h6>
                            </div>
                            <div class="card-body">
                                <p class="card-text">WordPress child theme with <a href="http://acceleratedemo.latashahussey.com/login" target="_blank">personalized login page</a> and custom post types.</p>
                                <div class="card-footer"><a class="btn btn-info" href="http://acceleratedemo.latashahussey.com/" target="_blank">View Live Site</a> <a class="btn btn-link teal-fg" href="https://github.com/latashahussey/acceleratedemo.latashahussey.com" target="_blank">Check code &gt;</a></div>
                            </div>
                            <!-- .card-body -->
                        </div>
                        <!-- .card-->
                    </div>
                    <!--.row-->
                </div>
                <!-- end Portfolio .container-->

                <!-- RECOMMENDATIONS-->
                <!-- Reference http://idangero.us/swiper/demos/03-vertical.html -->
                <div id="recommendations" class="jumbotron jumbotron-fluid edge--top--reverse edge--bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h4>What People Say About Me</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Recommendations</h3>
                                <hr />
                            </div>
                        </div>
                        <div class="row recommendation-wrapper">
                            <div class="col-md-4 social-links">
                                <h6>Eric Nathan</h6>
                                <p>Director, Online Faculty Support
                                    <br />at Lone Star College</p>
                                <a href="https://www.linkedin.com/in/dr-eric-nathan-956a6816" target="_black"><i class="fa fa-3x fa-linkedin"></i></a>
                            </div>
                            <div class="col-md-8 font-italic recommendation">
                                <p><img src="<?php echo get_template_directory_uri(); ?>/img/src/quotation-mark-blue.png">
                                    <p>LaTasha and I have collaborated on several projects in a Houston area higher education institution. These projects included: authoring interactive training material for faculty development; programming Java and JavaScript
                                        applications; creating websites with interactive training resources and multimedia productions. She also consistently exhibited the professionalism and expertise to complete all task successfully, and we met our
                                        goal for each project on time.</p>

                                    <p>LaTasha has also taught various programming courses within the college. She consistently leverages student feedback in an effort of improving the teaching and learning experience for both her and her students. Her students
                                        consistently rate her at a high level on end-of-course student feedback evaluations.</p>

                                    <p>I highly recommend LaTasha for any instructional technology, instructional design, web development and teaching position in the programming field.</p>
                            </div>
                        </div>
                    </div>
                    <!--- jumbotron.container end -->
                </div>
                <!--- .jumbotron end =-->


    </div>
    <!-- end of Recommendations .container-->


    <!-- CONTACT ME-->
    <?php // Use get_post() to grab the Contact page title and content
				      // Reference: https://developer.wordpress.org/reference/functions/get_post/
				                  $post = get_post(10, ARRAY_A);
				                  $title = $post['post_title'];
				                  $content = do_shortcode($post['post_content']);
				                 ?>
        <div id="contact" class="container-fluid edge--top--reverse edge--bottom pt-4 purplelight-bg pb-4">
            <div class="row">
                <div class="col-md-12 text-center white-fg">
                    <h4>Let's Talk</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 entry-title-wrapper white-fg">
                    <!--Post title-->
                    <h3 class="entry-title" itemprop="headline"><?php echo $title; ?></h3>
                    <hr />
                </div>
            </div>
            <!-- Display Contact Page Content-->
            <div class="container">
                <div class="row page">
                    <div class="col-md-12">
                        <div id="entry-content">
                            <div class="entry-content" itemprop="text">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end Contact me content .row-->
        </div>
        <!--- end Contact me .container =-->

        <!--KEEP UP TO DATE-->
        <?php // Use get_post() to grab the Keep Up to Date page title and content
				      // Reference: https://developer.wordpress.org/reference/functions/get_post/
				                  $post = get_post(22, ARRAY_A);
				                  $title = $post['post_title'];
				                  $content = do_shortcode($post['post_content']);
				                 ?>
            <div id="uptodate" class="container-fluid edge--top--reverse white-bg mt-4 pt-4 pb-4">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h4>Follow Me</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!--Post title-->
                        <h3 class="entry-title" itemprop="headline"><?php echo $title; ?></h3>
                        <hr />
                    </div>
                </div>
                <!-- Display Keep Up to Date Twitter Content-->
                <div class="row page">
                    <div class="col-md-6 offset-md-3">
                        <div id="entry-content">
                            <div class="entry-content" itemprop="text">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end Keep Up to Date content .row-->
            </div>
            <!--- end Keep Up to Date .container =-->
            </main>
            <!-- #main end-->
            </div>

            <!-- Wrapper end -->
            <?php get_footer(); ?>
