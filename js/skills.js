// Displays and animates charts in skills sections
jQuery(function() {
  jQuery('.chart').easyPieChart({
    scaleColor: "#F4F1F9",
    lineWidth: 5,
    lineCap: 'butt',
    barColor: '#366BFF',
    trackColor:	"#F4F1F9",
    size: 160,
    animate: 500
  });
});
