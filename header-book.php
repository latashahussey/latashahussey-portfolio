<?php /* Template Name: Book Header Template */
/**
 * The header for the Book landing page.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>



    </head>

    <body <?php body_class(); ?>>

        <div class="hfeed site" id="page">
            <div id="book" class="jumbotron jumbotron-fluid">
                <!---<div class="container">
                    <div class="row pb-5">
                        <div class="col-md-4">
                            <!--Branding
                            <h4 class="text-left">lh  LaTasha Hussey</h4>
                        </div>

                        <div class="col-md-8">
                            <!--Main Menu
                            <nav class="nav justify-content-end">
                                <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff
									 		    <div class="skip-link screen-reader-text">
												<a title="Skip to content" href="#content">Skip to content</a> </div>*/ ?>
                                    <?php wp_nav_menu( array( 'container_class' => 'main-menu', 'theme_location' => 'primary' ) ); ?>
                            </nav>
                        </div>
                    </div>
                </div> <!-- #end of .container-->

                <div class="container">
                    <div class="row mt-5 pb-5">
                        <!---Book Image & Buy Button -->
                        <div class="col-md-6 text-center">
                            <div class="row">
                                <div class="col-md-12">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/does-my-course-look-fat-cover.png" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <a href="http://a.co/dw5iKHf"target="_blank">
                                        <button type="button" class="btn btn-primary btn-xl lightblue-bg ">Find on Amazon</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Book Title & Description--->
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Book</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <h3 class="text-left">Does My Course Look Fat?</h3>
                                    <hr class="pull-left" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>When you ask someone whether you look fat, the answer inevitably depends on your relationship with them. An acquaintance won’t invest much in their answer, and a retail assistant will want you to make a purchase, but a good friend always tells the truth.</p>
                                    <p>This book is your new best friend. You hold in your hands a straightforward, honest guide to building a better course.</p>
                                    <p>LaTasha Hussey dishes the skinny on overweight online courses with five easy- to- implement steps, and she does so with wildly entertaining wit and your well-being in mind.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- #end container-->

            </div>
