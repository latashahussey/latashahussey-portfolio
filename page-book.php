<?php /* Template Name: Book Landing Page Template */
/**
 * The template for displaying the Front Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header('book');

$container   = get_theme_mod('understrap_container_type');
$sidebar_pos = get_theme_mod('understrap_sidebar_position');

?>

    <div class="wrapper" id="page-wrapper">
        <main class="site-main" id="main">

            <!--FIRST JUMBO QUOTE-->
            <div id="first-quote" class="jumbotron jumbotron-fluid edge--top edge--bottom--reverse mb-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2"><img class="pull-right" src="<?php echo get_template_directory_uri(); ?>/img/src/quotation-mark-green.png"></div>
                        <div class="col-md-10">
                            <blockquote class="text-center">
                                No difficult-to-digest or exotic ingredients. Every tip is designed for beginners and is surprisingly useful for the online course connoisseur.
                            </blockquote>
                        </div>
                    </div>
                </div>
                <!--- jumbotron.container end -->
            </div>
            <!--- .jumbotron end --->

            <!-- WHAT YOU'LL LEARN -->
            <div id="what-you-learn" class="container mb-5">
                <div class="row text-center ">
                    <div class="col-md-12">
                        <h4>Content</h4>
                    </div>
                </div>
                <div class="row text-center pb-5">
                    <div class="col-md-12">
                        <h3>What You'll Learn</h3>
                        <hr />
                    </div>
                </div>
				<!--First Row Learning Outcomes --->
                <div class="row pb-5">
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<h6><i class="fal fa-check mr-2"></i>Choose wisely</h6>
								<hr class="pull-left"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<p>How to stop overfeeding information to your students. </p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<h6><i class="fal fa-check mr-2"></i>Chunk it</h6>
								<hr class="pull-left"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<p>How to serve content in appealing, bite-sized chunks.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<h6><i class="fal fa-check mr-2"></i>Check it</h6>
								<hr class="pull-left"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<p>How to work out common course errors.</p>
							</div>
						</div>
					</div>
				</div>
				<!--Second Row Learning Outcomes --->
                <div class="row pb-5">
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<h6><i class="fal fa-check mr-2"></i>Change it</h6>
								<hr class="pull-left"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<p>How to make over your content to improve consistency.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<h6><i class="fal fa-check mr-2"></i>Cheat</h6>
								<hr class="pull-left"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<p>Where and how to find fat-course support.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<a href="http://a.co/dw5iKHf"><button type="button" class="btn btn-info btn-xl">Get the Book</button></a>
					</div>
				</div>
            </div>
            <!-- end What You'll Learn .container -->


            <!--SECOND QUOTE-->
            <div id="second-quote" class="jumbotron jumbotron-fluid edge--top--reverse edge--bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2"><img class="pull-right" src="<?php echo get_template_directory_uri(); ?>/img/src/quotation-mark-green.png"></div>
                        <div class="col-md-10">
                            <blockquote class="text-center">
                                No complicated or overly pretentious directions. When it comes to instruction, this guide leads by example. Do this, not that.
                            </blockquote>
                        </div>
                    </div>
                </div>
                <!--- jumbotron.container end -->
            </div>
            <!--- .jumbotron end --->

            <!-- IS THIS BOOK FOR ME-->
            <div id="book-for-me" class="jumbotron jumbotro-fluid pb-5 mb-5">
                <div class="row text-left ">
                    <div class="col-md-6 offset-md-6">
                        <h4>Who Should Read It</h4>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-6 offset-md-6">
                        <h3 class="text-left">Is the Book for Me?</h3>
                        <hr class="pull-left"/>
                    </div>
                </div>
                <div class="row text-left ">
					<div class="col-md-6 offset-md-6">
						<ul>
							<li>Do your students feel overwhelmed with content?</li>
							<li>Are you abusing the color red?</li>
							<li>Does your online course suffer from oversized fonts and obese paragraphs?</li>
							<li>Are your hyperlinks causing heartburn? If so, this book is for you. </li>
						</ul>
					</div>
                </div>
            </div> <!-- end Is This Book for Me .container -->

            <!-- WHAT READERS SAY-->
            <div id="readers-say" class="container mb-5">
                <div class="row text-center">
                    <div class="col-md-12">
                        <h4>Reviews</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3>What Readers Say</h3>
                        <hr />
                    </div>
                </div>
                <div class="row mt-5">
					<div class="col-md-6">
						<div class="row">
						  <div class="col-md-12">
						  	<img class="pull-left pr-1" src="<?php echo get_template_directory_uri(); ?>/img/quotation-mark-pink.png" />
							<p>'Does My Course's' straightforward approach explains how to organize online course material in a relevant and effective way. LaTasha presents the 5 Cs of healthy course planning with humor and a common sense approach. I laughed and nodded my head while already planning changes to my next online course. I look forward to becoming a better online instructor! </p>
						  </div>
						</div>
						<div class="row">
							<div class="col-md-4">
  							   <h6>Tara D. Wallace </h6>
  						  </div>
  						  <div class="col-md-8 pink-fg">
  							  <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
  						  </div>
						</div>


					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
								<img class="pull-left pr-1" src="<?php echo get_template_directory_uri(); ?>/img/quotation-mark-pink.png" />
								<p>This wonderful little book is filled with many great tips for both new and seasoned online instructors! Hussey shares key information in a fun, conversational tone that keeps you engaged and encourages you to take another look at your online course content. It's a quick read and the information you will gain is invaluable. I recommend this book to anyone who's interested in refining their online class.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								 <h6>Stephanie Moss </h6>
							</div>
							<div class="col-md-8 pink-fg">
								<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
							</div>
						</div>

					</div>
                </div>
				<div class="row">
				  	<div class="col-md-12 text-center">
				  		<a href="http://a.co/dw5iKHf"><button type="button" class="btn btn-primary btn-xl lightblue-bg" name="button">Get the Book</button></a>
				  	</div>
				</div>
            </div> <!-- end What Readers Say .container -->





        </main>
        <!-- #main end-->
    </div>

    <!-- Wrapper end -->
    <?php get_footer(); ?>
